﻿using AutoMapper;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class ProductHandler : IProductHandler
    {
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public ProductHandler(DBContext dBContext, IMapper mapper)
        {
            _dBContext = dBContext ;
            _mapper = mapper;
        }

        public async Task<Response> Create(ProductCreateUpdateModel param)
        {
            try
            {
                
               
                var product = await _dBContext.Products.Where(p => p.Name == param.Name).FirstOrDefaultAsync();

                product = new Product();

                product.Name = param.Name;

                product.Unit = param.Unit;

                _dBContext.Products.Add(product);

                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<ProductViewModel>(product);
                return new ResponseObject<ProductViewModel>(result);

            }
            catch (Exception ex)
            {
                Log.Error("Tạo mặt hàng không thành công");
                Log.Error("Param : @Param", param);
                return new Response<ProductViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> Delete(Guid id)
        {
           try
            {
                
                var product = await _dBContext.Products.Where(p => p.Id == id).FirstOrDefaultAsync();

                if (product == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Mặt hàng không tồn tại");

                _dBContext.Products.Remove(product);

                await _dBContext.SaveChangesAsync();

                return new ResponseDelete(HttpStatusCode.OK, "Xóa thành công", id, "");
            }
            catch(Exception ex)
            {
                Log.Error("Xóa mặt hàng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                var product = await _dBContext.Products.Where(p => p.Id == id).FirstOrDefaultAsync();

                if (product == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Mặt hàng không tồn tại");

                var result = _mapper.Map<ProductViewModel>(product);

                return new ResponseObject<ProductViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy mặt hàng thất bại");
                Log.Error("Id:@Id", id);
                return new Response<ProductViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> GetPageAsync(ProductQueryModel query)
        {
            try
            {
                var predicate = BuilQuery(query);

                var queryResult = _dBContext.Products.OrderBy(e => e.Name).Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<ProductViewModel>>(data);

                if (result != null) return new ResponsePagination<ProductViewModel>(result);

                else return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách mặt hàng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> Update(ProductCreateUpdateModel param, Guid id)
        {
            try
            {
                //Validate
                if (string.IsNullOrEmpty(param.Name) || param.Name.Length > 200)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên mặt hàng không được quá 200 từ và không được để trống");

                var product = await _dBContext.Products.Where(p => p.Id == id).FirstOrDefaultAsync();

                if (product == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Mặt hàng không tồn tại");

                _mapper.Map(param, product);
                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<ProductViewModel>(product);

                return new ResponseObject<ProductViewModel>(result);

            }
            catch(Exception ex)
            {
                Log.Error("Sửa không thành công");
                Log.Error("Param:@Param,Id:@Id", param,id);
                return new Response<ProductViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public Expression<Func<Product, bool>> BuilQuery(ProductQueryModel query)
        {
            var predicate = PredicateBuilder.New<Product>(true);
            if (query.Filter != "{}")
            {
                predicate.And(b => b.Name.ToLower() == query.Filter.ToLower());
            }
            return predicate;
        }
    }
}
