﻿using FluentAssertions;
using Supplies.Business;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTest
{
   
    public class ProductControllerTest:IntegrationTest
    {
        [Fact]
        public async Task Test1()
        {
            var product = new ProductCreateUpdateModel { Name = "Kem", Unit = "carton" };
            await Create_Delete(product);                    
        }

        [Fact]
        public async Task Test2()
        {
            var product = new ProductCreateUpdateModel { Name = "Bikeaa", Unit = "lason" };
            await Create_GetList(product);
           
        }
    }
}
