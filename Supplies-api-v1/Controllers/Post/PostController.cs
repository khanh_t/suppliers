﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers
{

    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/post")]
    [ApiExplorerSettings(GroupName = "Post")]
    public class PostController : ControllerBase
    {
        private readonly IPostHandler _postHandler;


        public PostController(IPostHandler postHandler)
        {

            _postHandler = postHandler;
        }

        /// <summary>
        /// Lấy danh sách Post theo phân trang
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("")]

        [ProducesResponseType(typeof(ResponsePagination<PostViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPageAsync([FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "")
        {
            var filterObject = JsonConvert.DeserializeObject<EmployeeQueryModel>(filter);
            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _postHandler.GetPageAsync(filterObject);

            return Helper.TransformData(result);
        }
    }
}
