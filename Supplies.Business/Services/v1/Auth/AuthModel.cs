﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace Supplies.Business
{ 
   public class LoginViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public virtual DepartmentViewModel Department { get; set; }
    }
    public class LoginModel
    {
        public string Email { get; set; }
        public string PassWord { get; set; }
    }

    public class ChangePasswordModel
    {
        public string OldPassWord { get; set; }
        public string NewPassWord { get; set; }
        public string RePassWord { get; set; }
    }
}
