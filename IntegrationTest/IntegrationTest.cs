﻿using Microsoft.AspNetCore.Mvc.Testing;
using Supplies.Data;
using Supplies_api_v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Threading.Tasks;
using Supplies.Common;
using Supplies.Business;
using Microsoft.AspNetCore.Mvc;

using FluentAssertions;
using System.Net;

namespace IntegrationTest
{
    public class IntegrationTest
    {
        protected readonly HttpClient TestClient;

        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>();

            TestClient = appFactory.CreateClient();
        }


        protected async Task Create_Delete(ProductCreateUpdateModel model)
        {
            var uri = "/api/v1/product";

            var created = await TestClient.PostAsJsonAsync(uri, model);

            var product = await created.Content.ReadAsAsync<Response<ProductViewModel>>();

            var response = await TestClient.GetAsync(uri + "/" + product.Data.Id.ToString());

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            await TestClient.DeleteAsync(uri + "/" + product.Data.Id);

            response = await TestClient.GetAsync(uri + "/" + product.Data.Id.ToString());

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        protected async Task Create_GetList(ProductCreateUpdateModel model)
        {
            var uri = "/api/v1/product";

            var response = await TestClient.PostAsJsonAsync(uri, model);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

           var response1 = await TestClient.GetAsync(uri);

            response1.StatusCode.Should().Be(HttpStatusCode.OK);

            (await response.Content.ReadAsAsync<Pagination<ProductViewModel>>()).Should().NotBeNull();

        }

    }
}
