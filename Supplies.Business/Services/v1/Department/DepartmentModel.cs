﻿
using AutoMapper;
using Bogus;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using Supplies.Common;
using Swashbuckle.AspNetCore.SchemaBuilder;
using Swashbuckle.AspNetCore.SwaggerGen;

using System;
using System.Collections.Generic;
using System.Net;

namespace Supplies.Business
{
    public class DepartmentViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public virtual List<EmployeeViewModelForDepartment> Employees { get; set; }
    }

    public class DepartmentCreateModel
    {

        public string Name { get; set; }
        public string Leader { get; set; }
    }
    public class DepartmentUpdateModel
    {
        public string Name { get; set; }
        public string Leader { get; set; }

    }
    public class DepartmentQueryModel : PaginationRequest
    {

    }

    public class DepartmentViewModelForEmployee
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }
        public DateTime CreatedOnDate { get; set; }

    }

    public class ExampleSchemaFilter : ISchemaFilter
    {
        private readonly IOpenApiObjectBuilder _objectBuilder;

        public ExampleSchemaFilter(IOpenApiObjectBuilder objectBuilder)
        {
            _objectBuilder = objectBuilder;
        }

        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (context.Type == typeof(DepartmentCreateModel))
            {
                var faker = new Faker<DepartmentCreateModel>().RuleFor(f => f.Name, f => f.Name.FullName())
                                                              .RuleFor(f => f.Leader, f => f.Name.FullName())                                                          
                                                              .Generate();
                schema.Example = _objectBuilder.Build(faker);

            }
            if (context.Type == typeof(Response<DepartmentViewModel>))
            {
                var faker = new Faker<DepartmentViewModel>().RuleFor(f => f.Name, f => f.Name.FullName())
                                                            .RuleFor(f => f.CreatedOnDate, f => DateTime.Now)
                                                            .RuleFor(f => f.Leader, f => f.Name.FullName())
                                                            .RuleFor(f => f.Id, f => f.Random.Guid())
                                                            .Generate();
                var response = new Response<DepartmentViewModel>
                {
                    Code =HttpStatusCode.OK,
                    Data = faker
                };
              
                schema.Example = _objectBuilder.Build(response);
            }
        }

        /*  public void Apply(OpenApiSchema schema, SchemaFilterContext context)
          {
              var faker = new Faker<DepartmentViewModel>().RuleFor(f => f.Name, f => f.Name.FullName())
                                                  .RuleFor(f => f.Leader, f => f.Name.FullName())
                                                  .RuleFor(f => f.Id, f => f.Random.Guid()).Generate();
              var department = new DepartmentCreateModel();
              if (context.Type == typeof(DepartmentCreateModel))
              {

                  schema.Example = new OpenApiObject()
                  {
                      [nameof(department.Name)] = new OpenApiString(faker.Name),
                      ["leader"] = new OpenApiString(faker.Leader),
                  };
              }

              if (context.Type == typeof(Response<DepartmentViewModel>))
              {
                  schema.Example = new OpenApiObject()
                  {
                      ["code"] = new OpenApiInteger(200),
                      ["message"] = new OpenApiString("Successful"),
                      ["totalTime"] = new OpenApiInteger(0),
                      ["data"] = new OpenApiObject()
                      {
                          ["id"] = new OpenApiString(faker.Id.ToString()),
                          ["name"] = new OpenApiString(faker.Name),
                          ["leader"] = new OpenApiString(faker.Leader),
                      }
                  };

              }

          }*/
    }
}
