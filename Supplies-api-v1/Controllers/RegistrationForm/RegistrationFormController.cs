﻿    using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers
{

    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/registrationform")]
    [ApiExplorerSettings(GroupName = "Registration Form")]
    public class RegistrationFormController : ControllerBase
    {
        private readonly IRegistrationFormHandler _formHandler;
        public RegistrationFormController(IRegistrationFormHandler formHandler)
        {
            _formHandler = formHandler ;
        }

        /// <summary>
        /// Lấy danh sách Đăng Kí theo phân trang
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<RegistrationFormViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPageAsync([FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "+")
        {
            var filterObject = JsonConvert.DeserializeObject<QueryModel>(filter);
            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _formHandler.GetPageAsync(filterObject);

            return Helper.TransformData(result);
        }


        /// <summary>
        /// Thêm Đăng Kí
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<RegistrationFormViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] RegistrationFormCreateUpdateModel model)
        {

            var result = await _formHandler.Create(model);
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Lấy thông tin Đăng Kí
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<RegistrationFormViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var result = await _formHandler.GetById(id);
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Sửa thông tin Đăng Kí 
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<RegistrationFormViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Update([FromBody] RegistrationFormCreateUpdateModel model, Guid id)
        {
            var result = await _formHandler.Update(model, id);
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Xóa Đăng Kí 
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _formHandler.Delete(id);
            return Helper.TransformData(result);
        }

    }
}
