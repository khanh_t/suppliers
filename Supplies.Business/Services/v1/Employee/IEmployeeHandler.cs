﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IEmployeeHandler
    {
        Task<Response> GetPageAsync(EmployeeQueryModel query);
        Task<Response> GetById(Guid id);
        Task<Response> Create(EmployeeCreateUpdateModel param);
        Task<Response> Update(EmployeeCreateUpdateModel param,Guid id);
        Task<Response> Delete(Guid id);
    }
}
