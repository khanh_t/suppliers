﻿using AutoMapper;
using Bogus;
using Microsoft.OpenApi.Any;
using NpgsqlTypes;
using Supplies.Business;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.AutoMapperConfig
{
    public class AutoMapperConfig :Profile
    {
        public AutoMapperConfig()
        {
            


            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            CreateMap<DepartmentSummary, SummaryDepartmentViewModel>().ReverseMap();

            CreateMap<Post,PostViewModel>().ReverseMap();


            CreateMap<RegistrationForm,RegistrationFormViewModel>().ReverseMap();
            CreateMap<RegistrationFormCreateUpdateModel, RegistrationForm>().ReverseMap();



            CreateMap<ListProduct,ProductListModel>().ReverseMap();
            CreateMap<ProductListCreateModel, ListProduct>().ReverseMap();




            CreateMap<Employee, EmployeeViewModel>();
            CreateMap<Employee, EmployeeViewModelForDepartment>();
            CreateMap<Employee, LoginViewModel>();
            CreateMap<EmployeeCreateUpdateModel, Employee>();

            CreateMap<Department, DepartmentViewModel>().ReverseMap();
            CreateMap< Department,DepartmentViewModelForEmployee > ().ReverseMap();
            CreateMap<DepartmentCreateModel, Department>().ReverseMap();
            CreateMap<DepartmentUpdateModel, Department>();

            CreateMap<Product, ProductViewModel>();
            CreateMap<ProductCreateUpdateModel, Product>();



        }

    }
}
