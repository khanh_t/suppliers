﻿
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Supplies.Business
{


    public class RegistrationFormViewModel
    {
        public Guid Id { get; set; }
        public EmployeeViewModel Employee { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public ICollection<ProductListModel> listProducts { get; set; }
    }

   public class RegistrationFormCreateUpdateModel
    {

        public Guid EmployeeId { get; set; }
        public ICollection<ProductListCreateModel> listProducts { get; set; } = new List<ProductListCreateModel>();
    }

    public class QueryModel : PaginationRequest
    {

    }

    public class ProductListModel
    {
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }
    }
    public class ProductListCreateModel
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }





}
