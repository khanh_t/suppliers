﻿using Microsoft.AspNetCore.Http;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IDepartmentHandler
    {
        Task<Response> GetPageAsync(DepartmentQueryModel query);
        Task<Response> GetById(Guid id);
        Task<Response> Create(DepartmentCreateModel param);
        Task<Response> Update(DepartmentUpdateModel param, Guid id);
        Task<Response> Delete(Guid id);
        Task<Response> Readfile();
    }
}
