﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class SummaryViewModel
    {
        public int Month { get; set; }

        public int Year { get; set; }

        public DateTime? LastModifiedOnDate { get; set; }

        public int Total { get; set; }
        
    }
    public class SummaryDepartmentViewModel
    {

        public Guid DepartmentId { get; set; }
        public string Department { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public int TotalProduct { get; set; }
    }
    public class SummaryProductViewModel
    { 

        public string Product { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public int TotalProduct { get; set; }
    }
}
