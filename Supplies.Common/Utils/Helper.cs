﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Primitives;
using Serilog;

namespace Supplies.Common
{

   
    public class Helper
    {
        
        /// <summary>
        /// Transform data to http response
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ActionResult TransformData(Response data)
        {
            var result = new ObjectResult(data) { StatusCode = (int)data.Code };
            return result;
        }
        /// <summary>
        /// Check if a string is a guid or not
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static bool IsGuid(string inputString)
        {
            try
            {
                var guid = new Guid(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Get user info in token and headder
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static RequestUser GetRequestInfo(HttpRequest request)
        {
        
            try
            {
                var result = new RequestUser
                {
                    Id = Guid.Empty,
                    UserName = "",
                };
                request.Headers.TryGetValue("X-Permission", out StringValues currentToken);
                if (string.IsNullOrEmpty(currentToken))
                {
                    var token = request.Headers["Authorization"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        var tokenString = token.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries);
                        if (tokenString.Length > 1)
                        {
                            currentToken = tokenString[1]?.Trim();
                        }
                    }
                }


                var key = Encoding.ASCII.GetBytes("ahdjsu*sudak^sdahsd#asdj)dja*ds2");
                var handler = new JwtSecurityTokenHandler();
                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidIssuer = "http://localhost:5000",
                    ValidAudience = "http://localhost:5000",
                };
                var currentUser = handler.ValidateToken(currentToken, validations, out var tokenSecure);
                //UserId
                if (currentUser.HasClaim(c => c.Type =="ID"))
                {
                    var userId = currentUser.Claims.FirstOrDefault(c => c.Type == "ID")?.Value;
                    if (!string.IsNullOrEmpty(userId) && IsGuid(userId))
                    {
                        result.Id = new Guid(userId);
                    }
                }
                else
                {
                    request.Headers.TryGetValue("ID", out StringValues userId);
                    if (!string.IsNullOrEmpty(userId) && IsGuid(userId))
                    {
                        result.Id = new Guid(userId);
                    }
                }
                //UserName
                if (currentUser.HasClaim(c => c.Type == "UserName"))
                {
                    var userName = currentUser.Claims.FirstOrDefault(c => c.Type == "UserName")?.Value;
                    if (!string.IsNullOrEmpty(userName))
                    {
                        result.UserName = userName;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                throw;

            }
        }
        public class RequestUser
        {
            public Guid Id { get; set; }
            public string UserName { get; set; }
            public string Name { get; set; }
        }
        private static readonly string[] VietnameseSigns = new string[]
        {

            "aAeEoOuUiIdDyY",

            "áàạảãâấầậẩẫăắằặẳẵ",

            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

            "éèẹẻẽêếềệểễ",

            "ÉÈẸẺẼÊẾỀỆỂỄ",

            "óòọỏõôốồộổỗơớờợởỡ",

            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

            "úùụủũưứừựửữ",

            "ÚÙỤỦŨƯỨỪỰỬỮ",

            "íìịỉĩ",

            "ÍÌỊỈĨ",

            "đ",

            "Đ",

            "ýỳỵỷỹ",

            "ÝỲỴỶỸ"
        };

        public static string RemoveSign4VietnameseString(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }
    }
}