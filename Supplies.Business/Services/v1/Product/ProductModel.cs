﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Supplies.Common;

namespace Supplies.Business
{
  public class ProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
    }
    public class ProductCreateUpdateModel
    {
        public string Name { get; set; }
        public string Unit { get; set; }

    }
   
    public class ProductQueryModel:PaginationRequest
    {

    }
}
