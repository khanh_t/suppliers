﻿    using Microsoft.EntityFrameworkCore;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Data
{
    [Table("ListProduct")]
    public class ListProduct: BaseTableDefault
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("RegistrationFormId")]
        public Guid RegistrationFormId { get; set; }
        public virtual RegistrationForm RegistrationForm { get; set; }
         
        [ForeignKey("ProductId")]
        public Guid ProductId { get; set; }
        public virtual  Product Product { get; set; }
        public int Quantity { get; set; }
        
    }
}
