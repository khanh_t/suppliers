﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers
{

    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/product")]
    [ApiExplorerSettings(GroupName ="Product")]
    public class ProductController:ControllerBase
    {
        public readonly IProductHandler _productHandler;

        public ProductController(IProductHandler productHandler)
        {
            _productHandler = productHandler ;
        }

        /// <summary>
        /// Lấy danh sách Mặt Hàng theo phân trang
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<ProductViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPageAsync([FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string? sort = "")
        {
            var filterObject = JsonConvert.DeserializeObject<ProductQueryModel>(filter);
            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _productHandler.GetPageAsync(filterObject);

            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy thông tin Mặt Hàng
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet,Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<ProductViewModel>),StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var result = await _productHandler.GetById(id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm Mặt Hàng
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<ProductViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] ProductCreateUpdateModel model)
        {
            var result = await _productHandler.Create(model);
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Sửa thông tin Mặt Hàng
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<ProductViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Update([FromBody] ProductCreateUpdateModel model,Guid id)
        {
            var result = await _productHandler.Update (model,id);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa Mặt Hàng 
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpDelete,Route("{id}")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _productHandler.Delete(id);
            return Helper.TransformData(result);
        }
    }
}
