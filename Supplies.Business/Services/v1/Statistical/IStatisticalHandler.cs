﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IStatisticalHandler
    {
        Task<Response> GetStatisticEmployeeByMonth();
        Task<Response> GetStatisticEmployeeByDepartment();
        Task<Response> GetStatisticFormByMonth();
       
    }
}
