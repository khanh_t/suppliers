﻿using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IProductHandler
    {
        Task<Response> GetPageAsync(ProductQueryModel query);
        Task<Response> GetById(Guid id);
        Task<Response> Create(ProductCreateUpdateModel param);
        Task<Response> Update(ProductCreateUpdateModel param, Guid id);
        Task<Response> Delete(Guid id);
    }
}
