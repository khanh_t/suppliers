﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Common
{
    public class BaseTableDefault
    {

        public DateTime LastModifiedOnDate { get; set; } = DateTime.Now;

        public DateTime CreatedOnDate { get; set; } = DateTime.Now;
    }
}
