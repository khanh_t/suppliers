﻿using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IAuthHandler
    {
        Task<Response> Authenticate(LoginModel param);
        Task<Response> Changepassword(ChangePasswordModel param);
    }
}
