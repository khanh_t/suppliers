﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/auth")]
    [ApiExplorerSettings(GroupName = "Auth")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthHandler _authHandler;

        public AuthController(IAuthHandler authHandler)
        {
            _authHandler = authHandler;
        }

        /// <summary>
        /// Đăng nhập
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous,HttpPost,Route("login")]
        [ProducesResponseType(typeof(ResponseObject<LoginViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var result = await _authHandler.Authenticate(model);
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Đổi mật khẩu
        /// </summary>
        /// <param name="model">Dữ liệu bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize,HttpPut,Route("changepassword")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult > ChangePassword([FromBody] ChangePasswordModel model)
        {

            var result = await _authHandler.Changepassword(model);

                return Helper.TransformData(result);
        }
    }
}
